const express = require('express');
const app = express();
const port = 3000;

app.get('/', (request, response) => {
    response.send('Hello from Express!');
});

var friendsArray = [];

var fs = require('fs');

try {  
    var data = fs.readFileSync('friends.txt', 'utf8');
    var line = data.toString().split("\r\n");
    for (var i = 0; i < line.length; i++) {
        var lineData = line[i].toString().split(";");
        if(lineData.length==3){
            var id = lineData[0];
            var name = lineData[1];
            var age = lineData[2];
            var friend = {id:id,name:name,age:age};
            friendsArray.push(friend);
        }
        console.log(line[i]);
    }
    //console.log(data.toString());
} catch(e) {
    console.log('Error:', e.stack);
}

app.get('/add', (request, response) => {
    var id = request.query.id-0;
    var name = request.query.name;
    var age = request.query.age-0;
    //todo: verify all 3 parameters look valid.
    if(id<1 || name.length<1 || age<1){
        response.send("error");
        return;
    }

    var friend = {id:id,name:name,age:age};
    friendsArray.push(friend);
    saveToFile();
    response.send(`Person ${id} added`);
});

app.get('/delete', (request, response) => {
    var id = request.query.id-0;
    for (var i = 0; i < friendsArray.length; i++) {
        if(friendsArray[i].id == id){
            friendsArray.splice(i, 1);
            saveToFile();
            response.send(`Person ${id} deleted`);
            return;
        }
    }
    response.send(`error cannot find ${id}`);
});

app.get('/fetchall', (request, response) => {
    var strHtml = "";
    for (var i = 0; i < friendsArray.length; i++) {
        strHtml +="<option value="+ friendsArray[i].id +">"+friendsArray[i].id+": "+friendsArray[i].name+" is "+friendsArray[i].age+" y/o</option>";
    }    
    response.send(`${strHtml}`);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(`server is listening on ${port}`);
});

function saveToFile(){
    var fs = require('fs');
    var stream = fs.createWriteStream("friends.txt");
    var strTofile ="";
    stream.once('open', function(fd) {
        for (var i = 0; i < friendsArray.length; i++) {
            strTofile += friendsArray[i].id+";"+friendsArray[i].name+";"+friendsArray[i].age+"\r\n";
        } 
      stream.write(strTofile);
      stream.end();
    });
}

