const express = require('express');
const fs = require('fs');
const app = express();

const port = 3000;
const jsonDataFile = 'triplist.json';

var tripsArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(tripsArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        tripsArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/add', (request, response) => {
    var id = request.query['id'];
    var passport = request.query['passport'];
    var code = request.query['code'];
    var city = request.query['city'];
    var depdate = request.query['depdate'];
    var retdate = request.query['retdate'];
    
    console.log(`/add id=${id}, passport=${passport}, code=${code}, city=${city} , depdate=${depdate}, retdate=${retdate}`);
    if (id == undefined || passport == '' || code == '' || city == '' || depdate == '' || retdate == '') {
        response.send("Error: invalid data sent");
        return;
    }

    var trip = {id: id, passport: passport, code: code, city: city, depdate: depdate, retdate: retdate};
    tripsArray.push(trip);
    saveAllData();
    response.send(`Item ${id} added!`);
});

app.get('/delete', (request, response) => {
    var id = request.query['id'] - 0;
    for (var i = 0; i < tripsArray.length; i++) {
        if (tripsArray[i].id == id) {
            tripsArray.splice(i, 1);
            saveAllData();
            response.send(`Item ${id} deleted`);
            return;
        }
    }
    response.send(`error deleting item, id=${id} not found`);
});

app.get('/getlist', (request, response) => {
    var keyword = request.query['keyword'];
    var html = "";
    for (var i = 0; i < tripsArray.length; i++) {
        var trip = tripsArray[i];
        if(keyword == undefined )
            html += `<option value="${trip.id}">${trip.id}: ${trip.passport} travels to ${trip.city}(${trip.code}) on ${trip.depdate} return on ${trip.retdate}</option>`;
        else{
            keyword = keyword.toLowerCase();
            if(trip.passport.toLowerCase().includes(keyword) || trip.code.toLowerCase().includes(keyword) || trip.city.toLowerCase().includes(keyword))
                html += `<option value="${trip.id}">${trip.id}: ${trip.passport} travels to ${trip.city}(${trip.code}) on ${trip.depdate} return on ${trip.retdate}</option>`;
        }            
    }
    response.send(html);
});

app.get('/getlistastable', (request, response) => {
    var tripscount = {};
    var html = "<table><tbody id='statistic'>";
    for (var i = 0; i < tripsArray.length; i++) {
        if (tripscount[tripsArray[i].code] == undefined) {
            tripscount[tripsArray[i].code] = 1;
        } else {
            tripscount[tripsArray[i].code]++;
        }
    }
    //console.log(tripscount);
    Object.keys(tripscount).forEach(function (key) {
        html += "<tr><td class='code'>"+key+"</td><td class='totalnumber'>"+tripscount[key]+"</td></tr>";
    });
    
    html += "</tbody></table>";
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});

