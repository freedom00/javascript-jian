var http = require('http');
var count = 0;
//create a server object:
http.createServer(function (req, res) {
  res.write('Hello World!'+ (++count)); //write a response to the client
  res.end(); //end the response
}).listen(5678); //the server object listens on port 5678